# Array of the applications to install
# 
# To add an application simply find the application string from https://chocolatey.org and add it to the array.
# To remove an application simply delete, or comment out, a line. 
#
# The applications listed here are simply a list of potentially useful applications that have been used by the author.

$apps = @(
  "adobereader",
  # "ant",
  # "asciidoctorj",
  # "blender",
  # "citrix-receiver",
  # "docker-desktop",
  # "eclipse",
  # "enpass.install",
  "firefox",
  # "gimp",
  # "git",
  # "gitkraken",
  "googlechrome",
  # "googleearthpro",
  # "inkscape",
  # "intellijidea-community",
  # "jdk11",
  # "keybase",
  # "libreoffice-fresh",
  # "microsoft-teams",
  # "mpv",
  # "nextcloud-client",
  # "notepadplusplus",
  "openjdk",
  # "pip",
  # "python",
  # "ruby",
  # "slack",
  # "treesizefree",
  # "virtualbox",
  # "vlc",
  # "vscode",
  "vscodium",
  "wsl",
  # "yed",
  # "zotero-standalone"
  "7zip"
)
