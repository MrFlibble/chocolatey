# Ensure that the script is running as Administrator
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
  "Not running as Administrator. Executing new shell."
  Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs;
  exit
}

# Check is the choco command is available. Upgrade or install.
$installed = Get-Command choco

if ($installed) {
  iex "choco upgrade -y all"
} else {
  "Chocolatey is not installed. Failed!!"
}

# Print out something to say we're done.
""
"Complete."
pause
