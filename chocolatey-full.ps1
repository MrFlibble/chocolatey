﻿# Ensure that the script is running as Administrator
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
 "Not running as Administrator. Executing new shell."
 Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs;
 exit
}

# Include array of all the applications to install from apps file
. "$PSScriptRoot\chocolatey-apps.ps1"

# Get the list of currently installed chocolatey apps
$installed = choco list -l | Where {$_ -notmatch 'packages|Did|Chocolatey'} | foreach { ($_ -split ' ')[0] }

# Figure out the difference between the apps list and the installed apps to determine what to install/uninstall/upgrade
$remove = $installed

foreach ($a in $apps) {
    $remove = $remove -ne $a
}


# Print out message to inform the user what changes will be made
" Install/Upgrade the following:"
"================================"
""
$apps
""

" Uninstall the following:"
"=========================="
"   (note dependencies will be listed but not uninstalled if still required)"
""
$remove
""


"Press Crtl-C to exit, or"
pause




# Check is the choco command is available. Upgrade or install.
$installed = Get-Command choco
if ($installed) {
  "Chocolatey already installed. Upgrading."
  choco upgrade -y chocolatey
} else {
  "Installing Chocolatey."
  iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}


# Un-Install all of the applications in the array
$appstring = "$remove"
iex "choco uninstall -y -x $appstring"

# Install all of the applications in the array
$appstring = "$apps"
iex "choco upgrade -y $appstring"


# Print out something to say we're done.
""
"Chocolatey and all applications are installed/upgrades/uninstalled. Have a nice day!."
pause
