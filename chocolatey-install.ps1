# Ensure that the script is running as Administrator
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
  "Not running as Administrator. Executing new shell."
  Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs;
  exit
}

# Include array of all the applications to install from apps file
. "$PSScriptRoot\chocolatey-apps.ps1"
"Preparing to install the following:"
$apps
""
"Press Crtl-C to exit, or"
pause

# Check is the choco command is available. Upgrade or install.
$installed = Get-Command choco
if ($installed) {
  "Chocolatey already installed. Upgrading."
  choco upgrade chocolatey
} else {
  "Installing Chocolatey."
  iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

# Install all of the applications in the array
$appstring = "$apps"
iex "choco install -y $appstring"

# Print out something to say we're done.
""
"Chocolatey and all applications are installed. Have a nice day!."
pause
