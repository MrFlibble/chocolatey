# Overview

This project hosts some simple scripts to install [Chocolatey](https://chocolatey.org), the package manager for Windows, and some useful applications.
The scripts provide a useful wrapper around Chocolatey to allow a complete installation from a single script.
Simply
* download the scripts,
* edit the apps list, and
* run the installation script.

# Script Usage

## Installation

Edit the `chocolatey-apps.ps1` file to set the list of applications to install. Then run either `chocolatey-full.ps1` or `chocolatey-install.ps1` to install Chocolatey and the listed applications. If using the full script this will check to see what applications have previously been installed by Chocolatey and upgrade any still listed, uninstall any that have been removed and install any newly listed.

## Upgrade

Run the `chocolatey-upgrade.ps1` script to upgrade Chocolatey and all apps installed by Chocolatey.

## Uninstall

To uninstall individual applications simply remove, or comment out, applications from the `chocolatey-apps.ps1` file, then run the `chocolatey-full.ps1` script. 
NOTE: This will also upgrade any remaining applications.

If you only want to uninstall the applications remove them from the app list and use the standard Chocolatey commands to uninstall the applications.
